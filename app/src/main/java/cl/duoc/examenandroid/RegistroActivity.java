package cl.duoc.examenandroid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistroActivity extends AppCompatActivity
{
    private EditText txtUser, txtPass, txtPass1;
    private Button btnRegistrarse;
    private FirebaseAuth mAuth;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_registro);
        btnRegistrarse = (Button)findViewById(R.id.btnRegistrarse);
        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Registrarse();
            }
        });
    }

    public void Registrarse()
    {
        txtUser = (EditText) findViewById(R.id.txtUser);
        txtPass = (EditText)findViewById(R.id.txtPass);
        txtPass1 = (EditText)findViewById(R.id.txtPass1);
        String email = txtUser.getText().toString();
        String password = txtPass1.getText().toString();
        if(txtUser.getText().toString() != "" && txtPass.getText().toString() != "" && txtPass1.getText().toString()!= "")
        {
            if(txtPass.getText().toString().equals(txtPass1.getText().toString()))
            {
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        Toast.makeText(RegistroActivity.this, ""+task+"", Toast.LENGTH_SHORT).show();
                        if (task.isSuccessful())
                        {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        }
                        else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            updateUI(null);
                        }
                    }
                });
            }
            else
            {
                Toast.makeText(this, "Error, contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, "Error, campos no pueden ir vacios", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateUI(FirebaseUser user)
    {
        if (user != null)
        {
            startActivity(new Intent(RegistroActivity.this, MainActivity.class));
        }
        else
        {
            Toast.makeText(this, "Error al registrarse", Toast.LENGTH_SHORT).show();
        }
    }
}
